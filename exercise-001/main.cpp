#include <fmt/format.h>
#include <fmt/chrono.h>
#include <stdlib.h>
#include <CLI11.hpp>
#include <random>

int main(int argc, char **argv)
{
  unsigned int counter = 20;
  std::vector<unsigned int> rand_stuff;

  /**
   * CLI11 is a command line parser to add command line options
   * More info at https://github.com/CLIUtils/CLI11#usage
   */

  CLI::App app{ "exercise-001" };
  try {
    app.add_option("-c,--count", counter, "Elements in Vector");
    app.parse(argc, argv);
  } catch (const CLI::ParseError &e) {
    return app.exit(e);
  }

  /**
   * The {fmt} lib is a cross platform library for printing and formatting text
   * it is much more convenient than std::cout and printf
   * More info at https://fmt.dev/latest/api.html 
   */

  fmt::print("Hello, {}!\n", app.get_name());

  /* INSERT YOUR CODE HERE */

  std::random_device dev;
  std::mt19937 rng(dev());
  std::uniform_int_distribution<std::mt19937::result_type> randNumber(1, 10);

  for (unsigned int i = 0; i < counter; i++) {
    rand_stuff.push_back(randNumber(rng));
  }

  fmt::print("unsorted vector:\n");

  for (auto j = rand_stuff.begin(); j < rand_stuff.end(); j++) 
    fmt::print("{} ", *j);
  

  auto start = std::chrono::system_clock::now();

  std::sort(rand_stuff.begin(), rand_stuff.end());

  auto end = std::chrono::system_clock::now();
  auto elapsed = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
  unsigned int time = elapsed.count();

  fmt::print("\n\nsorted vector:\n");

  for (auto j = rand_stuff.begin(); j < rand_stuff.end(); j++) 
    fmt::print("{} ", *j);
  
  fmt::print("\n\nsorting time: {}microseconds", time);
 

  return 0; /* exit gracefully*/
}
